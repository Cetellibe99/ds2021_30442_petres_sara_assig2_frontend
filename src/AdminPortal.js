import React, { Component } from 'react';
import { variables } from './Variables.js';
import {Link } from 'react-router-dom'

export class AdminPortal extends Component {
    constructor(props) {
        super(props);
        debugger;

        this.state = {
            users: [],
            UserId:"",
            Username: "",
            Address: "",
            BirthDate: ""
        }
    }
    DeleteUser(event) {
      
        var userId =event.currentTarget.id;
        debugger;
        fetch(variables.API_ADMINPORTAL_DELETECLIENT_URL + "/" + userId, {
            method: 'delete',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                id: userId,
            }
            )
        })
            .then((Response) => Response.json())
            .then((Result) => {

                if (Result.Status == 'Success') {
                    alert('Client was succesfully deleted');
                   // this.props.history.push('/adminportal');
                   window.location.reload(); 
                    debugger;
                }
                else {
                    var result = Result.Status;
                    alert('Some error has occured: ' + result);
                    debugger;
                }
            })
    }

    refreshList() {
        fetch(variables.API_ADMINPORTAL_URL)
            .then(response => response.json())
            .then(data => {
                this.setState({ users: data })
            })
    }


    componentDidMount() {
        this.refreshList();
    }



    render() {

        const {
            users,
            UserId,
            Username,
            Address,
            BirthDate
        } = this.state;

        return (
            <div>
               <Link to="/createclient"> <button type="button" className="btn btn-primary float-start create-user-button">Create client</button></Link>
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th>
                                Username
                            </th>
                            <th>
                                Address
                            </th>
                            <th>
                                Birthdate
                            </th>
                            <th></th>
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        {users.map(user =>
                            <tr key={user.Id}>
                                <td>{user.Username}</td>
                                <td>{user.Address}</td>
                                <td>{user.BirthDate}</td>
                                <td>
                                    <Link to={`/editclient/${user.Id}`}><button type="button" className="btn btn-outline-info btn-md">Edit</button></Link>
                                </td>
                                {/* <td><button type="button" id={`${user.Id}`} className="btn btn-outline-dark btn-md" onMouseEnter={this.DeleteUser.bind(this)}>Delete</button></td> */}
                                <td>
                                <Link to={`/userdevices/${user.Id}`}> <button type="button" className="btn btn-success btn-md">Devices</button></Link>
                                </td>
                            </tr>
                        )}
                    </tbody>

                </table>
           
            </div>
        )
    }

}