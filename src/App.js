import logo from './logo.svg';
import './App.css';
import { Home } from './Home';
import { Test } from './Test';
import { Login } from './Login';
import { RegisterUser } from './RegisterUser';
import { RegisterAdmin } from './RegisterAdmin';
import { AdminPortal } from './AdminPortal';
import { CreateClient } from './CreateClient';
import { EditClient } from './EditClient';
import { UserDevices } from './UserDevices';
import { CreateUserDevice } from './CreateUserDevice';
import { EditUserDevice } from './EditUserDevice';
import { EditUserSensor } from './EditUserSensor';
import { ClientPortal } from './ClientPortal';
import { ClientChart } from './ClientChart';
import { Line } from "react-chartjs-2";
import { BrowserRouter, Route, Switch, NavLink } from 'react-router-dom';

  function App () {

  return (
    
    <BrowserRouter>
      <div className="App container">
        <h3 className="d-flex justify-content-center m-3">
          Energy Utility Platform
        </h3>
       

        <nav className="navbar navbar-expand-sm bg-light navbar-dark">
          <ul className="navbar-nav">
            <li className="nav-item m-1">
              <NavLink className="btn btn-light btn-outline-primary" to="/login">
                Login
              </NavLink>
            </li>
            <li className="nav-item m-1">
              <NavLink className="btn btn-light btn-outline-primary" to="/registeruser">
                Register User
              </NavLink>
            </li>
            <li className="nav-item m-1">
              <NavLink className="btn btn-light btn-outline-primary" to="/registeradmin">
                Register Admin
              </NavLink>
            </li>
           
          </ul>
        </nav>
        <Switch>
          <Route path="/home" component={Home} />
          <Route path="/test" component={Test} />
          <Route path="/login" component={Login} />
          <Route path="/registeruser" component={RegisterUser} />
          <Route path="/registeradmin" component={RegisterAdmin} />
          <Route path="/adminportal" component={AdminPortal} />
          <Route path="/createclient" component={CreateClient} />
          <Route path="/editclient/:id" component={EditClient} />
          <Route path="/userdevices/:id" component={UserDevices} />
          <Route path="/createdevice/:id" component={CreateUserDevice} />
          <Route path="/editdevice/:id" component={EditUserDevice} />
          <Route path="/editusersensor/:id" component={EditUserSensor} />
          <Route path="/clientportal/:id" component={ClientPortal} />
          <Route path="/clientchart/:id" component={ClientChart} />
        </Switch>
      </div>
    </BrowserRouter>
     
  );
}

export default App;
