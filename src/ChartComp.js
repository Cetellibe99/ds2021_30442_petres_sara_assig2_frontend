import React from "react";
import { MDBContainer } from "mdbreact";
import { Line } from "react-chartjs-2";
  
const ChartComp = (propChartData) => {
  
  let hours = Object.entries(propChartData)[0][1]['Hours'];
  let values = Object.entries(propChartData)[0][1]['Values'];
  debugger;
  const data = {
    
    labels: values,
    datasets: [
      {
        label: "Oy - Hour of the day,  Ox - sensor value",
        data: hours,
        fill: true,
        backgroundColor: "rgba(6, 156,51, .3)",
        borderColor: "#02b844",
      }
    ]
  }
  
  return (
    <MDBContainer>
      <Line data={data} />
    </MDBContainer>
  );
}
  
export default ChartComp;