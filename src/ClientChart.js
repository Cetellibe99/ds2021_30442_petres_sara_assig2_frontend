import React, { Component } from 'react';
import DatePicker from "react-datepicker";
import 'react-datepicker/dist/react-datepicker.css';
import ReactDOM from 'react-dom';
import { Line } from "react-chartjs-2";
import ChartComp from './ChartComp.js';
import { variables } from './Variables.js';

export class ClientChart extends Component {

    constructor (props) {
        super(props)
        this.state = {
          startDate: new Date(),
          chartData: []
        };
        debugger;
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
      }

      handleChange(date) {
        this.setState({
          startDate: date
        });

        var deviceId = this.props.match.params.id;
        var thisDate = date.toDateString();
        debugger;
        fetch(variables.API_CLIENTCHART_URL + "/" +deviceId, {
            method: 'post',
            headers:{
                'Accept' : 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                id: deviceId,
                selectedDate: thisDate
                
            }
            )
            
        })
        .then((Response) => Response.json())
          .then((Result) => {
              this.handleLoadData(Result);
              
          })


      }

      handleLoadData(chartData){
        this.setState({
            chartData: chartData
          });

      }
    
      handleSubmit(e) {
        e.preventDefault();
        let main = this.state.startDate
        debugger;
      }

      render() {

        return (
          <div className = "container">
              <br/>
            <h3>Monitored energy consumption</h3><br/>
            <form onSubmit={ this.handleSubmit }>
              <div className="form-group datepicker-container">
                <label>Select Date: </label>
                <DatePicker
                   selected={ this.state.startDate }
                   onChange={ this.handleChange }
                  name="startDate"
                  dateFormat="MM-dd-yyyy"
                />
              </div>
              {/* <div className="form-group">
                  <br/>
                <button className="btn btn-success">View sensor data</button>
              </div> */}
            </form>
            <div className="energy-chart">
            <ChartComp propChartData = {this.state.chartData} />;
            </div>
          </div>
        );

        
      }

      
    

}