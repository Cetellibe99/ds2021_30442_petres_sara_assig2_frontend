import React, { Component } from 'react';
import { variables } from './Variables.js';
import { Link } from 'react-router-dom'
import Notification from './Notification.js'

export class ClientPortal extends Component {
    constructor(props) {
        super(props);
        debugger;

        this.state = {
            devices: [],
            UserId: "",
            DeviceId: "",
            Description: "",
            Location: "",
            MaximumEnergyConsumption: "",
            AverageEnergyConsumption: "",
            SensorId: "",
            SensorDescription: "",
            SensorMaximumValue: "",
        }
    }

    refreshList() {
        var clientId = this.props.match.params.id;
        
        fetch(variables.API_CLIENTPORTAL_URL + "/" + clientId)
            .then(response => response.json())
            .then(data => {
                this.setState({ devices: data })
            });

        this.changeUserId(clientId);
    }

    changeUserId(clientId) {
        let copy = this.state.UserId;
        copy = clientId;
        this.setState({ UserId: copy });
    }

    componentDidMount() {
        this.refreshList();
    }

    render() {

        const {
            devices,
            UserId,
            DeviceId,
            Description,
            Location,
            MaximumEnergyConsumption,
            AverageEnergyConsumption,
            SensorId,
            SensorDescription,
            SensorMaximumValue,
        } = this.state;

        return (
            <div>
                <br /><br />
                <h2>My devices</h2><br /><br />
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th>
                                Device Id
                            </th>
                            <th>
                                Description
                            </th>
                            <th>
                                Location
                            </th>
                            <th>
                                Max energy consumption
                            </th>
                            <th>
                                Avg energy consumption
                            </th>
                            <th>
                                Sensor's description
                            </th>
                            <th>
                                Sensor's max value
                            </th>
                            <th>

                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        {devices.map(device =>
                            <tr key={device.DeviceId}>
                                <td>{device.DeviceId}</td>
                                <td>{device.Description}</td>
                                <td>{device.Location}</td>
                                <td>{device.MaximumEnergyConsumption}</td>
                                <td>{device.AverageEnergyConsumption}</td>
                                <td>{device.SensorDescription}</td>
                                <td>{device.SensorMaximumValue}</td>
                                <td> <Link to={`/clientchart/${device.DeviceId}`}><button type="button" className="btn btn-danger btn-md">View chart</button></Link></td>
                            </tr>
                        )}
                    </tbody>

                </table>
                <Notification userId= {this.state.UserId} />
            </div>
        )
    }
}