import React, { Component } from 'react';
import { variables } from './Variables.js';

export class CreateClient extends Component {

    constructor(){
        super();

        this.state = {
            Username: '',
            Address: '',
            Birthdate: '',
            Password: '',
        }

        this.Username = this.Username.bind(this);
        this.Address = this.Address.bind(this);
        this.Birthdate = this.Birthdate.bind(this);
        this.Password =this.Password.bind(this);
    }

    Username(event){
        
        this.setState({Username: event.target.value})
    }

    Address(event){
        this.setState({Address: event.target.value})
    }

    Birthdate(event){
        this.setState({Birthdate: event.target.value})
    }

    Password(event){
        this.setState({Password: event.target.value})
    }


    createClient(event){
        debugger;

        fetch(variables.API_ADMINPORTAL_CREATECLIENT_URL, {
            method: 'post',
            headers:{
                'Accept' : 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
               Username: this.state.Username,
                Address: this.state.Address,
                Birthdate: this.state.Birthdate,
                Password: this.state.Password
            }
            )
            
        })
        .then((Response) => Response.json())
          .then((Result) => {
              debugger;
              if(Result.Status == 'Success'){
                  alert('Client was succesfully created');
                  this.props.history.push('/adminportal');
                  

              }
              else{
                  debugger;
                  var result = Result.Status;
                  alert('Some error has occured: ' + result);
              }
          })
    }

    render() {
        return (
            <div className="row ">
            <div className="col-3"></div>
            <div className="col-6 auth-container">
            <form>
             <br/>
                <h3>Create new client</h3>

                <div className="form-group">
                    <label className="auth-label">Username</label>
                    <input type="text" className="form-control auth-input" placeholder="Username" onChange={this.Username.bind(this)} />
                </div>
                <div className="form-group">
                    <label className="auth-label">Address</label>
                    <input type="text" className="form-control auth-input" placeholder="Address" onChange={this.Address.bind(this)} />
                </div>
                <div className="form-group">
                    <label className="auth-label">Birthdate</label>
                    <input type="date" className="form-control auth-input" onChange={this.Birthdate.bind(this)}/>
                </div>
                <div className="form-group">
                    <label className="auth-label">Password</label>
                    <input type="password" className="form-control auth-input" placeholder="Password" onChange={this.Password.bind(this)}/>
                </div>
                <button type="submit" className="btn btn-primary btn-block login-button" onMouseEnter={this.createClient.bind(this)}>Create client</button>
            </form>
            <br/>
            </div>
            <div className="col-3"></div>
            </div>
        )
    }

}