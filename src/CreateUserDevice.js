import React, { Component } from 'react';
import { variables } from './Variables.js';
import {Link } from 'react-router-dom'

export class CreateUserDevice extends Component {

    constructor(){
        super();

        this.state = {
            Description: '',
            Location: '',
            MaximumEnergyConsumption: '',
            AverageEnergyConsumption: '',
            SensorDescription: '',
            SensorMaximumValue: '',
        }

        this.Description = this.Description.bind(this);
        this.MaximumEnergyConsumption = this.MaximumEnergyConsumption.bind(this);
        this.Location = this.Location.bind(this);
        this.AverageEnergyConsumption =this.AverageEnergyConsumption.bind(this);
        this.SensorDescription = this.SensorDescription.bind(this);
        this.SensorMaximumValue = this.SensorMaximumValue.bind(this);
    }

    
    Description(event){
        
        this.setState({Description: event.target.value})
    }

    MaximumEnergyConsumption(event){
        this.setState({MaximumEnergyConsumption: event.target.value})
    }

    Location(event){
        this.setState({Location: event.target.value})
    }

    AverageEnergyConsumption(event){
        this.setState({AverageEnergyConsumption: event.target.value})
    }
    SensorDescription(event){
        this.setState({SensorDescription: event.target.value})
    }
    SensorMaximumValue(event){
        this.setState({SensorMaximumValue: event.target.value})
    }

    createDevice(event){
        debugger;
        var clientId = this.props.match.params.id;

        fetch(variables.API_USERDEVICES_CREATE_URL, {
            method: 'post',
            headers:{
                'Accept' : 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                Description: this.state.Description,
                MaximumEnergyConsumption: this.state.MaximumEnergyConsumption,
                Location: this.state.Location,
                AverageEnergyConsumption: this.state.AverageEnergyConsumption,
                SensorMaximumValue: this.state.SensorMaximumValue,
                SensorDescription: this.state.SensorDescription,
                UserId : clientId,
            }
            )
            
        })
        .then((Response) => Response.json())
          .then((Result) => {
              debugger;
              if(Result.Status == 'Success'){
                 
                  alert('Device was succesfully created');
                  this.props.history.push(`/userdevices/${clientId}`);
                  

              }
              else{
                  debugger;
                  var result = Result.Status;
                  alert('Some error has occured: ' + result);
              }
          })
    }

    render() {
        return (
            <div className="row ">
            <div className="col-3"></div>
            <div className="col-6 auth-container">
            <form>
             <br/>
                <h3>Create new device for client</h3>

                <div className="form-group">
                    <label className="auth-label">Description</label>
                    <input type="text" className="form-control auth-input" placeholder="Description" onChange={this.Description.bind(this)} />
                </div>
                <div className="form-group">
                    <label className="auth-label">Location</label>
                    <input type="text" className="form-control auth-input" placeholder="Location" onChange={this.Location.bind(this)} />
                </div>
                <div className="form-group">
                    <label className="auth-label">MaximumEnergyConsumption</label>
                    <input type="text" className="form-control auth-input" placeholder="MaximumEnergyConsumption" onChange={this.MaximumEnergyConsumption.bind(this)}/>
                </div>
                <div className="form-group">
                    <label className="auth-label">AverageEnergyConsumption</label>
                    <input type="text" className="form-control auth-input" placeholder="AverageEnergyConsumption" onChange={this.AverageEnergyConsumption.bind(this)}/>
                </div>
                <div className="form-group">
                    <label className="auth-label">Sensor description</label>
                    <input type="text" className="form-control auth-input" placeholder="Sensor description" onChange={this.SensorDescription.bind(this)}/>
                </div>
                <div className="form-group">
                    <label className="auth-label">Sensor maximum value</label>
                    <input type="text" className="form-control auth-input" placeholder="Sensor maximum value" onChange={this.SensorMaximumValue.bind(this)}/>
                </div>
                <button type="submit" className="btn btn-primary btn-block login-button" onMouseEnter={this.createDevice.bind(this)}>Create device</button>
            </form>
            <br/>
            </div>
            <div className="col-3"></div>
            </div>
        )
    }

}