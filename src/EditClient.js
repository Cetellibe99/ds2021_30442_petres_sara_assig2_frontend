import React, { Component } from 'react';
import { variables } from './Variables.js';
import { Link } from 'react-router-dom';

export class EditClient extends Component {

    constructor(props) {
        super(props);

        this.state = {
            userData: "",
            Username: "",
            BirthDate: "",
            Address: ""
        }
    }

    refreshList() {
        var clientid = this.props.match.params.id;

        fetch(variables.API_ADMINPORTAL_EDITCLIENT_URL + "/" + clientid)
            .then(response => response.json())
            .then(data => {
                this.setState({ userData: data })
            });
    }

    componentDidMount() {
        this.refreshList();
    }

    Username(event) {
        let copy = this.state.userData;
        copy.Username = event.currentTarget.value;
        this.setState({ userData: copy });
    }

    Address(event) {
        let copy = this.state.userData;
        copy.Address = event.currentTarget.value;
        this.setState({ userData: copy });
    }

    BirthDate(event) {
         let copy = this.state.userData;
         copy.BirthDate = event.currentTarget.value;
         debugger;
         this.setState({ userData: copy });
    }

    EditUser(event) {
        var clientid = this.props.match.params.id;

        fetch(variables.API_ADMINPORTAL_EDITCLIENT_URL + "/" + clientid, {
            method: 'post',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                Username: this.state.userData.Username,
                Address: this.state.userData.Address,
                BirthDate: this.state.userData.BirthDate,
            }
            )

        })
            .then((Response) => Response.json())
            .then((Result) => {

                if (Result.Status == 'Success') {
                    alert('Client was succesfully edited');
                    this.props.history.push('/adminportal');
                }
                else {
                    var result = Result.Status;
                    alert('Some error has occured: ' + result);
                }
            })
    }

    render() {

        var {
            userData,
            Username,
            BirthDate,
            Address,
        } = this.state;

        return (
            <div className="row ">
                <div className="col-3"></div>
                <div className="col-6 auth-container">
                    <form>
                        <br />
                        <h3>Edit client</h3>

                        <div className="form-group">
                            <label className="auth-label">Username</label>
                            <input type="text" className="form-control auth-input" value={userData.Username} onChange={this.Username.bind(this)} />
                        </div>
                        <div className="form-group">
                            <label className="auth-label">Address</label>
                            <input type="text" className="form-control auth-input" value={userData.Address} onChange={this.Address.bind(this)} />
                        </div>
                        <div className="form-group">
                            <label className="auth-label">Birthdate</label>
                            <input type="date" className="form-control auth-input" value={userData.BirthDate} onChange={this.BirthDate.bind(this)} />
                        </div>
                        <button type="submit" className="btn btn-primary btn-block login-button" onMouseEnter={this.EditUser.bind(this)}>Submit</button>
                    </form>
                    <br />
                </div>
                <div className="col-3"></div>
            </div>
        )
    }
}