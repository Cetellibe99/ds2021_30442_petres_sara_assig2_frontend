import React, { Component } from 'react';
import { variables } from './Variables.js';
import { Link } from 'react-router-dom'

export class EditUserDevice extends Component {

    constructor(props) {
        super(props);

        this.state = {
            deviceData: "",
            UserId: "",
            Description: "",
            Location: "",
            MaximumEnergyConsumption: "",
            AverageEnergyConsumption: ""
        }
    }
    refreshList() {
        var deviceId = this.props.match.params.id;

        fetch(variables.API_USERDEVICES_EDIT_URL + "/" + deviceId)
            .then(response => response.json())
            .then(data => {
                this.setState({ deviceData: data })
            });
    }
    componentDidMount() {
        this.refreshList();
    }

    Description(event) {
        let copy = this.state.deviceData;
        copy.Description = event.currentTarget.value;
        this.setState({ deviceData: copy });
    }

    Location(event) {
        let copy = this.state.deviceData;
        copy.Location = event.currentTarget.value;
        this.setState({ deviceData: copy });
    }

    MaximumEnergyConsumption(event) {
        let copy = this.state.deviceData;
        copy.MaximumEnergyConsumption = event.currentTarget.value;
        this.setState({ deviceData: copy });
    }

    AverageEnergyConsumption(event) {
        let copy = this.state.deviceData;
        copy.AverageEnergyConsumption = event.currentTarget.value;
        this.setState({ deviceData: copy });
    }

    EditDevice(event) {
        var deviceId = this.props.match.params.id;
        var userId = this.state.deviceData.UserId;

        fetch(variables.API_USERDEVICES_EDIT_URL + "/" + deviceId, {
            method: 'post',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                Description: this.state.deviceData.Description,
                Location: this.state.deviceData.Location,
                AverageEnergyConsumption: this.state.deviceData.AverageEnergyConsumption,
                MaximumEnergyConsumption: this.state.deviceData.MaximumEnergyConsumption,
            }
            )

        })
            .then((Response) => Response.json())
            .then((Result) => {
                if (Result.Status == 'Success') {
                    alert('Device was succesfully edited');
                    this.props.history.push(`/userdevices/${userId}`);
                }
                else {
                    var result = Result.Status;
                    alert('Some error has occured: ' + result);
                }
            })
    }

    render() {

        var {
            deviceData,
            Description,
            Location,
            AverageEnergyConsumption,
            MaximumEnergyConsumption,
            UserId
        } = this.state;

        return (
            <div className="row ">
                <div className="col-3"></div>
                <div className="col-6 auth-container">
                    <form>
                        <br />
                        <h3>Edit device</h3>
                        <div className="form-group">
                            <label className="auth-label">UserId</label>
                            <input type="text" readonly="true" className="form-control auth-input" value={deviceData.UserId} />
                        </div>

                        <div className="form-group">
                            <label className="auth-label">Location</label>
                            <input type="text" className="form-control auth-input" value={deviceData.Location} onChange={this.Location.bind(this)} />
                        </div>
                        <div className="form-group">
                            <label className="auth-label">Description</label>
                            <input type="text" className="form-control auth-input" value={deviceData.Description} onChange={this.Description.bind(this)} />
                        </div>
                        <div className="form-group">
                            <label className="auth-label">Maximum Energy Consumption</label>
                            <input type="text" readonly="true" className="form-control auth-input" value={deviceData.MaximumEnergyConsumption} onChange={this.MaximumEnergyConsumption.bind(this)} />
                        </div>
                        <div className="form-group">
                            <label className="auth-label">Average Energy Consumption</label>
                            <input type="text" readonly="true" className="form-control auth-input" value={deviceData.AverageEnergyConsumption} onChange={this.AverageEnergyConsumption.bind(this)} />
                        </div>
                        <button type="submit" className="btn btn-primary btn-block login-button" onMouseEnter={this.EditDevice.bind(this)}>Submit</button>
                    </form>
                    <br />
                </div>
                <div className="col-3"></div>
            </div>
        )
    }
}