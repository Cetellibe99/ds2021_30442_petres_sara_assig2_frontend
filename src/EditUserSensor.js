import React, { Component } from 'react';
import { variables } from './Variables.js';
import { Link } from 'react-router-dom'

export class EditUserSensor extends Component {

    constructor(props) {
        super(props);

        this.state = {
            sensorData: "",
            Id: "",
            DeviceId: "",
            Description: "",
            MaximumValue: "",
        }
    }

    refreshList() {
        var deviceId = this.props.match.params.id;

        fetch(variables.API_USERSENSORS_EDIT_URL + "/" + deviceId)
            .then(response => response.json())
            .then(data => {
                this.setState({ sensorData: data })
            });
    }
    componentDidMount() {
        this.refreshList();
    }

    Description(event) {
        let copy = this.state.sensorData;
        copy.Description = event.currentTarget.value;
        this.setState({ sensorData: copy });
    }

    MaximumValue(event) {
        let copy = this.state.sensorData;
        copy.MaximumValue = event.currentTarget.value;
        this.setState({ sensorData: copy });
    }

    EditSensor(event) {
        var deviceId = this.props.match.params.id;

        fetch(variables.API_USERSENSORS_EDIT_URL + "/" + deviceId, {
            method: 'post',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                Description: this.state.sensorData.Description,
                MaximumValue: this.state.sensorData.MaximumValue,
            }
            )

        })
            .then((Response) => Response.json())
            .then((Result) => {
                if (Result.Status == 'Success') {
                    alert('Sensor was succesfully edited');
                    window.location.reload();
                }
                else {
                    var result = Result.Status;
                    alert('Some error has occured: ' + result);
                }
            })
    }

    render() {

        var {
            sensorData,
            Id,
            DeviceId,
            Description,
            MaximumValue,
        } = this.state;

        return (
            <div className="row ">
                <div className="col-3"></div>
                <div className="col-6 auth-container">
                    <form>
                        <br />
                        <h3>Edit sensor</h3>
                        <div className="form-group">
                            <label className="auth-label">Id</label>
                            <input type="text" readonly="true" className="form-control auth-input" value={sensorData.Id} />
                        </div>
                        <div className="form-group">
                            <label className="auth-label">Device Id</label>
                            <input type="text" readonly="true" className="form-control auth-input" value={sensorData.DeviceId} />
                        </div>
                        <div className="form-group">
                            <label className="auth-label">Description</label>
                            <input type="text" className="form-control auth-input" value={sensorData.Description} onChange={this.Description.bind(this)} />
                        </div>
                        <div className="form-group">
                            <label className="auth-label">Maximum Value</label>
                            <input type="text" className="form-control auth-input" value={sensorData.MaximumValue} onChange={this.MaximumValue.bind(this)} />
                        </div>
                        <button type="submit" className="btn btn-primary btn-block login-button" onMouseEnter={this.EditSensor.bind(this)}>Submit</button>
                    </form>
                    <br />
                </div>
                <div className="col-3"></div>
            </div>
        )
    }
}