import React, { Component } from 'react';
import './Authentication.css';
import { variables } from './Variables.js';
import {HubConnectionBuilder, LogLevel} from '@microsoft/signalr';

export class Login extends Component {

    constructor(props) {
        super(props);

        this.state = {
            Username: '',
            Password: '',
        }

        this.Username = this.Username.bind(this);
        this.Password = this.Password.bind(this);
    }

    Username(event) {
        this.setState({ Username: event.target.value })
    }

    Password(event) {
        this.setState({ Password: event.target.value })
    }

    login(event) {
        debugger;

        fetch(variables.API_LOGIN_URL, {
            method: 'post',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                Username: this.state.Username,
                Password: this.state.Password
            }
            )

        })
            .then((Response) => Response.json())
            .then((Result) => {
                debugger;
                if (Result.Status == 200) {
                    if (Result.role == 'Admin') {
                        alert('You have successfully logged in. You will be redirected to the admin portal');
                        this.props.history.push('/adminportal');
                        debugger;
                    }
                    else if (Result.role == 'User') {
                        alert('You have successfully logged in. You will be redirected to the client portal');
                        this.props.history.push(`/clientportal/${Result.userId}`);
                        debugger;
                    }
                    else {
                        alert('Invalid role. You will not be able to login');
                        this.props.history.push('/login');
                        debugger;
                    }
                }
                else {
                    debugger;
                    var result = Result.Status;
                    alert('Some error has occured: ' + result);
                }
            })
    }

    render() {
        return (
            <div className="row ">
                <div className="col-3"></div>
                <div className="col-6 auth-container">
                    <form>
                        <br />
                        <h3>Login</h3>

                        <div className="form-group">
                            <label className="auth-label">Username</label>
                            <input type="text" className="form-control auth-input" placeholder="Username" onChange={this.Username.bind(this)} />
                        </div>
                        <div className="form-group">
                            <label className="auth-label">Password</label>
                            <input type="password" className="form-control auth-input" placeholder="Password" onChange={this.Password.bind(this)} />
                        </div>
                        <button type="submit" className="btn btn-primary btn-block login-button" onMouseEnter={this.login.bind(this)}>Submit</button>
                    </form>
                    <br />
                </div>
                <div className="col-3"></div>
            </div>
        )
    }
}