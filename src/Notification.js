import {HubConnectionBuilder, LogLevel, HttpTransportType} from '@microsoft/signalr';
import { variables } from './Variables.js';
import {toast} from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import React from "react";
toast.configure()

const Notification  = (userId) =>{
     
         try{
             const connection = new HubConnectionBuilder()
             .withUrl(variables.API_HUBCONNECTION_URL,{
                skipNegotiation: true,
                transport: HttpTransportType.WebSockets  
             })
             .configureLogging(LogLevel.Information)
             .build();

             connection.on('SendMessage', (message, currentClientId) => {
                 console.log("message received:", message, currentClientId, userId);

                 if((currentClientId + "") == (userId.userId + "")){
                     debugger;
                    toast.info(message, {position: toast.POSITION.BOTTOM_RIGHT, autoClose:6000, toastId:currentClientId + ""});
                 }
                 
             });

             connection.start();
         }
         catch(e){
             console.log(e);
         }

    return (
        <div></div>
    );
}

export default Notification;