import React, { Component } from 'react';
 import { variables } from './Variables.js';

export class RegisterAdmin extends Component {

    constructor(){
        super();

        this.state = {
            Username: '',
            Address: '',
            Birthdate: '',
            Password: '',
            AdminKey: ''
        }

        this.Username = this.Username.bind(this);
        this.Address = this.Address.bind(this);
        this.Birthdate = this.Birthdate.bind(this);
        this.Password =this.Password.bind(this);
        this.AdminKey =this.AdminKey.bind(this);
    }

    Username(event){
        
        this.setState({Username: event.target.value})
    }

    Address(event){
        this.setState({Address: event.target.value})
    }

    Birthdate(event){
        this.setState({Birthdate: event.target.value})
    }

    Password(event){
        this.setState({Password: event.target.value})
    }

    AdminKey(event){
        this.setState({AdminKey: event.target.value})
    }

    registerAsAdmin(event){

        fetch(variables.API_REGISTER_ADMIN_URL, {
            method: 'post',
            headers:{
                'Accept' : 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
               Username: this.state.Username,
                Address: this.state.Address,
                Birthdate: this.state.Birthdate,
                Password: this.state.Password,
                AdminKey: this.state.AdminKey
            }
            )
            
        })
        .then((Response) => Response.json())
          .then((Result) => {
              if(Result.Status == 'Success'){
                  alert('You have successfully registered. You can now log in.');
                  this.props.history.push('/login');
                  

              }
              else{
                  var result = Result.Status;
                  alert('Some error has occured: ' + result);
              }
          })
    }

    render() {
        return (
            <div className="row ">
            <div className="col-3"></div>
            <div className="col-6 auth-container">
            <form>
             <br/>
                <h3>Register as an admin</h3>

                <div className="form-group">
                    <label className="auth-label">Username</label>
                    <input type="text" className="form-control auth-input" placeholder="Username" onChange={this.Username.bind(this)} />
                </div>
                <div className="form-group">
                    <label className="auth-label">Address</label>
                    <input type="text" className="form-control auth-input" placeholder="Address" onChange={this.Address.bind(this)} />
                </div>
                <div className="form-group">
                    <label className="auth-label">Birthdate</label>
                    <input type="date" className="form-control auth-input"  onChange={this.Birthdate.bind(this)}/>
                </div>
                <div className="form-group">
                    <label className="auth-label">Password</label>
                    <input type="password" className="form-control auth-input" placeholder="Password" onChange={this.Password.bind(this)} />
                </div>
                <div className="form-group ">
                    <label className="auth-label">Secret key</label>
                    <input type="text" className="form-control auth-input secret-key-container" onChange={this.AdminKey.bind(this)} />
                </div>
                <button type="submit" className="btn btn-primary btn-block login-button" onMouseEnter={this.registerAsAdmin.bind(this)}>Submit</button>
            </form>
            <br/>
            </div>
            <div className="col-3"></div>
            </div>
        )
    }
   
}