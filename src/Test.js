import React, { Component } from 'react';
import { variables } from './Variables.js';

export class Test extends Component {
    constructor(props) {
        super(props);

        this.state = {
            tests: [],
            modalTitle: "",
            TestName: "",
            TestId: 0,
            TestAddress: ""
        }
    }

    refreshList() {
        fetch(variables.API_TEST_URL + 'get')
            .then(response => response.json())
            .then(data => {
                this.setState({ tests: data })
            })
    }

    componentDidMount() {
        this.refreshList();
    }

    changeTestName = (e) => {
        this.setState({ TestName: e.target.value })
    }
    addClick() {
        this.setState({
            modalTitle: "Add Test",
            TestId: 0,
            TestName: "",
            TestAddress: ""
        });
    }
    editClick(test) {
        this.setState({
            modalTitle: "Edit Test",
            TestId: test.TestId,
            TestName: test.TestName,
            TestAddress: test.TestAddress
        });
    }

    render() {

        const {
            tests,
            modalTitle,
            TestId,
            TestName,
            TestAddress
        } = this.state;

        return (
            <div>
                <button type="button" className="btn btn-primary m-2 float-end"
                    data-bs-toggle="modal"
                    data-bs-target="#exampleModal"
                    onClick={() => this.addClick()}>
                    Add Test

                </button>
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th>
                                TestId
                            </th>
                            <th>
                                TestName
                            </th>
                            <th>
                                TestAddress
                            </th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        {tests.map(test =>
                            <tr key={test.id}>
                                <td>{test.id}</td>
                                <td>{test.name}</td>
                                <td>{test.address}</td>
                                <td>
                                    <button type="button" className="btn btn-light mr-1"
                                        data-bs-toggle="modal"
                                        data-bs-target="#exampleModal"
                                        onClick={() => this.editClick(test)}>
                                        Button
                                    </button>
                                </td>
                            </tr>
                        )}
                    </tbody>

                </table>

                <div className="input-group mb-3">
                    <span className="input-group-text">TestName</span>
                    <input type="text" className="form-control"
                        value={TestName}
                        onChange={this.changeTestName}
                    ></input>

                </div>
                {TestId == 0 ?
                    <button type="button" className="btn btn-primary float-start">
                        Create
                    </button>
                    : null}

                {TestId != 0 ?
                    <button type="button" className="btn btn-primary float-start">
                        Update
                    </button>
                    : null}
            </div>
        )
    }
}