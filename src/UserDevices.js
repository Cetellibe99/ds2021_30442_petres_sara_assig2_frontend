import React, { Component } from 'react';
import { variables } from './Variables.js';
import { Link } from 'react-router-dom'

export class UserDevices extends Component {

    constructor(props) {
        super(props);

        this.state = {
            devices: [],
        }
    }

    getCreateUrl() {
        var clientId = this.props.match.params.id;
        var result = `/createdevice/${clientId}`;
        return result;

    }

    DeleteDevice(event) {

        var deviceId = event.currentTarget.id;
        fetch(variables.API_USERDEVICES_DELETE_URL + "/" + deviceId, {
            method: 'delete',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                id: deviceId,
            }
            )
        })
            .then((Response) => Response.json())
            .then((Result) => {

                if (Result.Status == 'Success') {
                    alert('Device was succesfully deleted');
                    window.location.reload();
                }
                else {
                    var result = Result.Status;
                    alert('Some error has occured: ' + result);
                }
            })
    }



    refreshList() {

        var clientId = this.props.match.params.id;
        fetch(variables.API_USERDEVICES_URL + "/" + clientId)
            .then(response => response.json())
            .then(data => {
                this.setState({ devices: data })
            })
    }


    componentDidMount() {
        this.refreshList();
    }

    render() {

        const {
            devices,
        } = this.state;

        return (
            <div>
                <Link to={`${this.getCreateUrl()}`}><button type="button" className="btn btn-primary float-start create-device-button ">Add new device</button></Link>

                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th>
                                Id
                            </th>
                            <th>
                                Description
                            </th>
                            <th>
                                Location
                            </th>
                            <th>
                                MaximumEnergyConsumption
                            </th>
                            <th>
                                AverageEnergyConsumption
                            </th>
                            <th></th>
                            <th></th>
                            <th>Sensor</th>
                        </tr>
                    </thead>
                    <tbody>
                        {devices.map(device =>
                            <tr key={device.Id}>
                                <td>{device.Id}</td>
                                <td>{device.Description}</td>
                                <td>{device.Location}</td>
                                <td>{device.MaximumEnergyConsumption}</td>
                                <td>{device.AverageEnergyConsumption}</td>
                                <td>
                                    <Link to={`/editdevice/${device.Id}`}><button type="button" className="btn btn-outline-info btn-md">Edit</button></Link>
                                </td>
                                <td>
                                    {/* <button type="button" id={`${device.Id}`} className="btn btn-outline-dark btn-md" onMouseEnter={this.DeleteDevice.bind(this)}>Delete</button>
                                 */}
                                </td>

                                <td>
                                    <Link to={`/editusersensor/${device.Id}/`}><button type="button" className="btn btn-danger btn-md">Sensor info</button></Link>

                                </td>

                            </tr>
                        )}
                    </tbody>

                </table>

            </div>
        )
    }

}