export const variables = {
    API_TEST_URL:"https://localhost:5001/api/tests/",
    API_LOGIN_URL:"https://localhost:5001/api/authenticate/login",
    API_REGISTER_USER_URL: "https://localhost:5001/api/authenticate/registeruser",
    API_REGISTER_ADMIN_URL: "https://localhost:5001/api/authenticate/registeradmin",
    API_ADMINPORTAL_URL: "https://localhost:5001/api/adminportal/index",
    API_ADMINPORTAL_CREATECLIENT_URL: "https://localhost:5001/api/adminportal/createclient",
    API_ADMINPORTAL_EDITCLIENT_URL: "https://localhost:5001/api/adminportal/editclient",
    API_ADMINPORTAL_DELETECLIENT_URL: "https://localhost:5001/api/adminportal/deleteclient",
    API_USERDEVICES_URL: "https://localhost:5001/api/devices/index",
    API_USERDEVICES_CREATE_URL: "https://localhost:5001/api/devices/create",
    API_USERDEVICES_EDIT_URL: "https://localhost:5001/api/devices/edit",
    API_USERDEVICES_DELETE_URL: "https://localhost:5001/api/devices/delete",
    API_USERSENSORS_EDIT_URL: "https://localhost:5001/api/sensors/edit",
    API_CLIENTPORTAL_URL: "https://localhost:5001/api/clientportal/index",
    API_CLIENTCHART_URL: "https://localhost:5001/api/clientportal/clientchart",
    API_HUBCONNECTION_URL: "https://localhost:5001/notificationHub",

}